import os
from pathlib import Path
from unittest.mock import MagicMock

import nibabel as nib
import numpy as np
import pytest
from nibabel.testing import data_path
from nipype import Workflow
from numpy.testing import assert_array_equal

import fw_gear_niftyreg
from fw_gear_niftyreg import main, niftyreg_workflow


@pytest.fixture
def example_nifti_image():
    return os.path.join(data_path, "example4d.nii.gz")


@pytest.fixture
def example_image_data(example_nifti_image):
    return nib.load(example_nifti_image).get_fdata()


def test_main(tmpdir, example_nifti_image, mocker):
    mock_load_image = mocker.patch("fw_gear_niftyreg.main.load_image")
    mock_volume_crop = mocker.patch("fw_gear_niftyreg.main.volume_crop")
    mock_build_wf = mocker.patch("fw_gear_niftyreg.main.build_wf")
    logging_mock = mocker.patch("logging.Logger.info")

    mock_work_dir = Path(tmpdir.mkdir("work"))
    mock_crop_folder = mock_work_dir / "subject_1"
    mock_wf_dir = mock_work_dir / "workflow"
    mock_output_dir = Path(tmpdir.mkdir("output"))

    example_data = np.array(
        [[9, 6, 8, 6], [12, 55, 4, 4], [23, 4, 6, 8], [2, 7, 9, 8]], np.int64
    )

    mock_input_files_path = {
        "ct_screening_nifti": example_nifti_image,
        "pet_screening_nifti": example_nifti_image,
        "ct_followup_nifti": example_nifti_image,
        "pet_followup_nifti": example_nifti_image,
    }

    mock_load_image.side_effect = [
        (example_data, (123, 23, 23, 2)),
        (example_data, (123, 60, 100, 2)),
    ]

    mock_build_wf.return_value = MagicMock(spec=Workflow)

    main.run(
        mock_input_files_path, "subject_1", "followup", mock_output_dir, mock_work_dir
    )

    logging_mock.assert_called_with("Niftyreg finished successfully...")


def test_main_with_exception(tmpdir, mocker):
    mock_load_image = mocker.patch("fw_gear_niftyreg.main.load_image")
    mock_volume_crop = mocker.patch("fw_gear_niftyreg.main.volume_crop")
    logging_mock = mocker.patch("logging.Logger.info")

    mock_work_dir = Path(tmpdir.mkdir("work"))
    mock_crop_folder = mock_work_dir / "subject_1"
    mock_wf_dir = mock_work_dir / "workflow"
    mock_output_dir = Path(tmpdir.mkdir("output"))

    example_data = np.array(
        [[9, 6, 8, 6], [12, 55, 4, 4], [23, 4, 6, 8], [2, 7, 9, 8]], np.int64
    )

    mock_input_path = tmpdir.mkdir("input")
    mock_empty_nifti_image = mock_input_path.join("file01.nii.gz")

    mock_input_files_path = {
        "ct_screening_nifti": mock_empty_nifti_image,
        "pet_screening_nifti": mock_empty_nifti_image,
        "ct_followup_nifti": mock_empty_nifti_image,
        "pet_followup_nifti": mock_empty_nifti_image,
    }

    mock_load_image.return_value = (example_data, (123, 23, 23, 2))

    with pytest.raises(SystemExit) as e:
        main.run(
            mock_input_files_path,
            "subject_1",
            "followup",
            mock_output_dir,
            mock_work_dir,
        )
    assert e.type == SystemExit
    assert e.value.code == 1

    logging_mock.assert_called_with("Exiting...")


def test_load_image(example_image_data, example_nifti_image):

    expected_image_shape = example_image_data.shape

    actual_image_data, actual_image_shape = main.load_image(example_nifti_image)

    assert_array_equal(actual_image_shape, expected_image_shape)
    assert_array_equal(actual_image_data, example_image_data)


def test_volume_crop(example_image_data, example_nifti_image, tmpdir):

    mock_crop_folder = Path(tmpdir.mkdir("crop"))
    actual_path = main.volume_crop(
        "subject01", 90, example_nifti_image, mock_crop_folder, "ct"
    )
    expected_path = mock_crop_folder / "ct_subject01_Followup.nii.gz"

    assert actual_path == expected_path

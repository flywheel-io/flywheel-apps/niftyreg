from pathlib import Path
from unittest import mock
from unittest.mock import MagicMock, Mock

import flywheel
import pytest
from flywheel_gear_toolkit import GearToolkitContext
from fw_gear_testing import (
    MockAcquisition,
    MockContainerMixin,
    MockFinder,
    MockSession,
    MockSubject,
)

from fw_gear_niftyreg import parser


@pytest.fixture(scope="function")
def mock_subject():
    """Mock a subject container"""

    def get_mock_subject(n_sessions=1, n_acquisitions=1, n_files=1):
        subject = MockSubject(label="test")
        sessions = []
        for j in range(n_sessions):
            session = MockSession(label="screening")
            acquisitions = []
            for k in range(n_acquisitions):
                acquisition = MockAcquisition(label=f"acq-{k}")
                files = []
                for l in range(n_files):
                    files.append(flywheel.FileEntry(name=f"file-{l}"))
                acquisition.files = files
                acquisitions.append(acquisition)
            session.acquisitions = MockFinder(acquisitions)
            sessions.append(session)
        subject.sessions = MockFinder(sessions)
        return subject

    return get_mock_subject


@pytest.fixture(scope="function")
def mock_session():
    """Mock a session container"""

    def get_mock_session(n_acquisitions=1, n_files=1):
        session = MockSession(label="followup")
        acquisitions = []
        for k in range(n_acquisitions):
            acquisition = MockAcquisition(label=f"acq-{k}")
            files = []
            for l in range(n_files):
                files.append(flywheel.FileEntry(name=f"file-{l}"))
            acquisition.files = files
            acquisitions.append(acquisition)
        session.acquisitions = MockFinder(acquisitions)
        return session

    return get_mock_session


@pytest.fixture(scope="function")
def mock_acquisition():
    """Mock an acquisition container"""

    def get_mock_acquisition(n_files=1):

        acquisition = MockAcquisition(label=f"acq-1")
        files = []
        for l in range(n_files):
            files.append(flywheel.FileEntry(name=f"file-{l}"))
        acquisition.files = files

        return acquisition

    return get_mock_acquisition


@pytest.fixture
def download_file_mock():
    return mock.patch("mock_acquisition.download_file")


@pytest.fixture
def mock_find_one_subject():
    return mock.patch("MockFinder.find_one")


def test_find_file_in_container(mock_session):

    # dir_name = parser.find_file_in_container(mock_session(), "abc", "/flywheel/v0")
    # assert dir_name == "abc"

    pass


def test_parse_input_filepath(
    mock_subject, mock_session, mock_find_one_subject, mocker
):
    pass

    # find_file_in_container = mocker.patch("parser.find_file_in_container")
    # mock_path_exists = mocker.patch("os.path.exists")
    # context = MagicMock(spec=dir(GearToolkitContext))
    #
    # mock_screening_label = "screening"
    # test_subject = mocker.patch("flywheel.Subject(label='test')")
    #
    # context.client.get_subject.return_value = mock_subject()
    # context.client.get_session.return_value = mock_session()
    #
    # mock_session.reload.return_value = ""
    #
    # context.return_value.work_dir.return_value = "flywheel/v0/work"
    # mock_path_exists.return_value = True
    # find_file_in_container.side_effect = [
    #     "flywheel/v0/inputs/ct_screen_path",
    #     "flywheel/v0/inputs/pt_screen_path",
    #     "flywheel/v0/inputs/ct_followup_path",
    # ]
    #
    # expected_input_files_path_dict = {
    #     "ct_screening_nifti": "flywheel/v0/inputs/ct_screen_path",
    #     "pt_screening_nifti": "flywheel/v0/inputs/pt_screen_path",
    #     "ct_followup_nifti": "flywheel/v0/inputs/ct_followup_path",
    # }
    # mock_parents = flywheel.models.container_parents.ContainerParents(
    #     subject="test", session="test_session"
    # )
    #
    # input_files_path = parser.parse_input_filepath(
    #     mock_parents, mock_screening_label, context
    # )
    # assert input_files_path == expected_input_files_path_dict


def test_parse_config():
    pass


def test_get_and_download_file(mock_acquisition, download_file_mock, mocker):

    expected_output = Path("/flywheel/input/test_nifti_file.nii.gz")

    test_output = parser.get_and_download_file(
        "tag_abc",
        flywheel.Acquisition(id="test-acq"),
        Path("/flywheel/input"),
        "test_nifti_file.nii.gz",
    )

    assert test_output == expected_output

import os
from pathlib import Path
from unittest import mock

import numpy as np
import pytest
from nibabel.testing import data_path
from nipype import Workflow

from fw_gear_niftyreg import niftyreg_workflow

#
# OUTPUT_DIR = (Path(__file__).parent / "output/").resolve()
# ASSETS_DIR = (Path(__file__).parent / "assets/").resolve()


@pytest.fixture
def example_nifti_image():
    return os.path.join(data_path, "example4d.nii.gz")


def test_split_string():
    test_outputs = "abc-def yyz-wer"

    output1, output2 = niftyreg_workflow.split_string(test_outputs)

    assert output1 == "abc-def"
    assert output2 == "yyz-wer"


def test_workflow_setup(tmpdir, example_nifti_image):
    mock_workflow = Path(tmpdir.mkdir("workflow"))
    mock_output = Path(tmpdir.mkdir("output"))

    actual_wf = niftyreg_workflow.build_wf(
        example_nifti_image,
        example_nifti_image,
        example_nifti_image,
        example_nifti_image,
        "subject_01",
        "session_01",
        mock_workflow,
        True,
        mock_output,
    )
    assert type(actual_wf) == type(Workflow(name="niftyreg", base_dir=mock_workflow))

"""Main module."""
import logging
import sys
from pathlib import Path

import nibabel as nib
import numpy as np

from .niftyreg_workflow import build_wf

log = logging.getLogger(__name__)

orientm = 2 * np.eye(4)
orientm[3, 3] = 1


def volume_crop(subject_label, cut, image_path, crop_folder, prefix):
    """

    Args:
        subject_label (str):
        cut (int):
        image_path (pathlib.Path):
        crop_folder (pathlib.Path):
        prefix (str):

    Returns:
        pathlib.Path Return path to the crop Nifti image.
    """
    image = nib.load(image_path).get_fdata()[:, :, ::-1]
    crop_arr = image[:, :, :cut]
    crop_arr = crop_arr[:, :, ::-1]

    crop_img = nib.Nifti1Image(crop_arr, orientm)

    crop_img_sp = crop_folder / f"{prefix}_{subject_label}_Followup.nii.gz"

    nib.save(crop_img, crop_img_sp)

    return crop_img_sp


def load_image(nifti_image_path):
    """

    Args:
        nifti_image_path pathlib.Path): Path of the nifti image.

    Returns:
        Tuple:
            tuple containing:
                    - (numpy.ndarray)
                    - (numpy.ndarray) shape of the nifti image

    """
    image_data = nib.load(nifti_image_path).get_fdata()
    image_shape = image_data.shape

    return image_data, image_shape


def run(
    input_files_path, subject_label, followup_session_label, output_folder, work_dir
):
    """Preparing Niftyreg workflow and run the workflow

    Args:
        input_files_path (dict): List of dictionary with input file path.
        subject_label (str): Subject label for naming output files.
        followup_session_label (str): Followup session container label.
        output_folder (pathlib.Path): Path of the output directory.
        work_dir (pathlib.Path): Path of the work directory.


    """

    ref_ct_path = Path(input_files_path["ct_screening_nifti"])
    flo_ct_path = Path(input_files_path["ct_followup_nifti"])

    ref_pet_path = Path(input_files_path["pet_screening_nifti"])
    flo_pet_path = Path(input_files_path["pet_followup_nifti"])

    crop_folder = work_dir / subject_label

    log.info("Creating a crop folder")
    crop_folder.mkdir(exist_ok=True, parents=True)

    wf_dir = (work_dir / "workflow").absolute()

    log.info("Loading input images")
    # reference and floating nifti
    ref_ct, ref_ct_size = load_image(ref_ct_path)
    flo_ct, flo_ct_size = load_image(flo_ct_path)
    #
    crop_flag = flo_ct_size[2] - ref_ct_size[2]

    if crop_flag >= 20:
        log.info("Preparing to create Niftyreg workflow")
        flo_ct_path = volume_crop(
            subject_label, ref_ct_size[2], flo_ct_path, crop_folder, "ct"
        )
        flo_pet_path = volume_crop(
            subject_label, ref_ct_size[2], flo_pet_path, crop_folder, "pet"
        )

    try:
        wf = build_wf(
            ref_ct_path,
            ref_pet_path,
            flo_ct_path,
            flo_pet_path,
            subject_label,
            followup_session_label,
            wf_dir,
            True,
            output_folder.parent.absolute(),
        )

        log.info("Running Niftyreg workflow")
        wf.run()

    except Exception:
        log.exception("Niftyreg failed")
        log.info("Exiting...")
        sys.exit(1)
    else:
        log.info("Niftyreg finished successfully...")

import logging

from nipype import MapNode, Node, Workflow
from nipype.interfaces import niftyreg
from nipype.interfaces.io import DataSink
from nipype.interfaces.utility import Function, Rename

log = logging.getLogger(__name__)


def split_string(avg_output):
    """Split output string into two seperate file

    Args:
        avg_output: Output string from reg_aladin and reg_f3d

    Returns:
        Tuple: Return path to affine matrix and followup file

    """
    avg_output_list = avg_output.split(" ")
    affine = avg_output_list[0]
    flo_image = avg_output_list[1]

    return affine, flo_image


def build_wf(
    ref_img,
    ref_pet_img,
    flo_img,
    flo_pet_img,
    subject_label,
    followup_session_label,
    wf_dir,
    compact_output=False,
    output_dir=None,
):
    """Build a nipype workflow.

    Args:
        ref_img (pathlib.Path) : Path to the reference CT image.
        ref_pet_img (pathlib.Path) : Path to the reference PET image.
        flo_img (pathlib.Path) : Path to the followup CT image.
        flo_pet_img (pathlib.Path) : Path to the followup PET image.
        subject_label (str): Subject container label.
        followup_session_label (str): Followup session container label.
        wf_dir (pathlib.Path) : Path to the nipype workflow directory.
        compact_output (bool): Default is false. True if compact output is desired.
        output_dir (pathlib.Path) : Path to the designated output direcotry if compact output is on.


    Returns:
        nipype.pipeline.engine.workflows.Workflow: Return a nipype Workflow object.
    """
    # Standard set of outputs for given label
    res_affine_img = f"ct_{subject_label}_{followup_session_label}_f2s.nii.gz"
    res_bspline_img = f"ct_{subject_label}_{followup_session_label}_f2s_bspline.nii.gz"
    affine_matrix = f"{subject_label}_affine_matrix.txt"
    bspline_matrix = f"{subject_label}_{followup_session_label}_bspline_matrix.nii.gz"
    bspline_pet_sp = f"pet_{subject_label}_{followup_session_label}_f2s_bspline.nii.gz"

    log.info("Creating a Niftyreg workflow...")
    niftyreg_wf = Workflow(name="niftyreg", base_dir=wf_dir)

    log.info("Creating Nodes for the workflow...")
    reg_aladin = Node(niftyreg.RegAladin(), name="reg_aladin")
    reg_f3d = Node(niftyreg.RegF3D(), name="reg_f3d")
    reg_resample = Node(niftyreg.RegResample(), name="reg_resample")

    # Utility Nodes
    rename_output = Node(Rename(), name="rename_output")
    rename_output.inputs.format_string = bspline_pet_sp

    log.info("Attaching inputs file to nodes")
    log.debug("Prepping reg_aladin....")

    try:
        reg_aladin.inputs.ref_file = ref_img
        reg_aladin.inputs.flo_file = flo_img
        reg_aladin.inputs.res_file = res_affine_img
        reg_aladin.inputs.aff_file = affine_matrix

        log.debug("Prepping reg_f3d....")
        reg_f3d.inputs.ref_file = ref_img
        reg_f3d.inputs.flo_file = flo_img
        reg_f3d.inputs.res_file = res_bspline_img
        reg_f3d.inputs.cpp_file = bspline_matrix

        log.debug("Prepping reg_resample....")
        reg_resample.inputs.ref_file = ref_pet_img
        reg_resample.inputs.flo_file = flo_pet_img
        reg_resample.inputs.inter_val = "CUB"

        log.info(f"Setting up Datasink")
        # Create DataSink object
        sinker = Node(DataSink(), name="sinker")

        log.debug("Connecting all the nodes")
        # Connect all nodes
        niftyreg_wf.connect(
            [
                (reg_aladin, reg_f3d, [("aff_file", "aff_file")]),
                (reg_f3d, reg_resample, [("cpp_file", "trans_file")]),
            ]
        )

        # Renaming reg_resample name to include given UID
        niftyreg_wf.connect([(reg_resample, rename_output, [("out_file", "in_file")])])

        # If compact output is turn on, only selected amount of output will be saved to designated directory
        if compact_output and output_dir:
            sinker.inputs.base_directory = str(output_dir)

            niftyreg_wf.connect(
                [
                    (
                        reg_aladin,
                        sinker,
                        [
                            ("aff_file", "output.@reg_aladin_aff"),
                            ("res_file", "output.@reg_aladin_res"),
                        ],
                    ),
                    (
                        reg_f3d,
                        sinker,
                        [
                            ("cpp_file", "output.@reg_f3d_cpp"),
                            ("res_file", "output.@reg_f3d_res"),
                        ],
                    ),
                    (rename_output, sinker, [("out_file", "output.@reg_resample")]),
                ]
            )
        else:
            sinker.inputs.base_directory = "./nodes_output"

            split_output = MapNode(
                Function(
                    input_names=["avg_output"],
                    output_names=["output1", "output2"],
                    function=split_string,
                ),
                name="split_output",
                iterfield=["avg_output"],
            )

            split_output_b = split_output.clone(name="split_output_b")

            niftyreg_wf.connect(
                [
                    (reg_aladin, split_output, [("avg_output", "avg_output")]),
                    (reg_f3d, split_output_b, [("avg_output", "avg_output")]),
                ]
            )
            niftyreg_wf.connect(
                [
                    (
                        reg_aladin,
                        sinker,
                        [
                            ("aff_file", "reg_aladin.affine"),
                            ("res_file", "reg_aladin.transformed_image"),
                        ],
                    ),
                    (
                        reg_f3d,
                        sinker,
                        [
                            ("cpp_file", "reg_f3d.cpp_file"),
                            ("res_file", "reg_f3d.resampled_file"),
                            ("invcpp_file", "reg_f3d.invcpp_file"),
                            ("invres_file", "reg_f3d.invres_file"),
                        ],
                    ),
                    (rename_output, sinker, [("out_file", "reg_resample.out_file")]),
                    (
                        split_output,
                        sinker,
                        [
                            ("output1", "reg_aladin.avg_output"),
                            ("output2", "reg_aladin.avg_output2"),
                        ],
                    ),
                    (
                        split_output_b,
                        sinker,
                        [
                            ("output1", "reg_f3d.avg_output"),
                            ("output2", "reg_f3d.avg_output2"),
                        ],
                    ),
                ]
            )
        log.debug("Completed setup the workflow...")
    except Exception as err:
        if "TraitError" in str(err.__class__):
            print("TraitError:", err)
        else:
            raise
    else:
        return niftyreg_wf

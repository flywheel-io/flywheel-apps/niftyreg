"""Parser module to parse gear config.json."""
import logging
import os

import flywheel

log = logging.getLogger(__name__)


def get_and_download_file(file_tag, acquisition_container, input_dir, file_name):
    """

    Args:
        file_tag (str): File tag from the file container.
        acquisition_container (flywheel.Acquisition): Flywheel Acquisition container.
        input_dir (pathlib.Path): Input directory.
        file_name (str): Name of the file.

    Returns:
        (pathlib.Path) Path to the file
    """
    try:
        log.info(
            f"Downloading file with {file_tag} tag from {acquisition_container.id}"
        )
        acquisition_container.download_file(file_name, input_dir / file_name)
    except Exception:
        log.exception("Error to download nifti file...")
    else:
        log.info(f"Downloaded file with {file_tag} tag.")
        return input_dir / file_name


def find_file_in_container(session_container, file_tag, input_dir):
    """

    Args:
        session_container (flywheel.Sessions): Flywheel Session container.
        file_tag (str): File tag from the file container.
        input_dir (pathlib.Path): Input directory.

    Returns:
        (pathlib.Path) Path to the file

    """

    session_container = session_container.reload()

    for acquisition in session_container.acquisitions.iter():
        acquisition = acquisition.reload()
        for file in acquisition.files:
            if file.tags:
                if file_tag in file.tags:
                    return get_and_download_file(
                        file_tag, acquisition, input_dir, file.name
                    )

    log.info(f"Can not locate file in {session_container.label} with {file_tag} tag.")
    return None


def parse_input_filepath(parents, screening_label, gear_context):
    """

    Args:
        parents (flywheel.ContainerParents): List of parents for the analysis container.
        screening_label (str): Label for the screening session.
        gear_context (GearToolkitContext):

    Returns:
        (Dict[str,Any]) input file paths.

    """
    input_files_path = dict()

    subject_container = gear_context.client.get_subject(parents.subject)
    followup_session_container = gear_context.client.get_session(parents.session)

    work_dir = gear_context.work_dir
    input_dir = work_dir / "gear_inputs"

    input_dir.mkdir(exist_ok=True, parents=True)

    # Get screening filepath
    try:
        screening_session = subject_container.sessions.find_one(
            f"label={screening_label}"
        )
    except flywheel.ApiException:
        log.exception(
            f"Found more than one session container with label- {screening_label}"
        )
    except Exception:
        log.exception("Uncaught exception")
    else:
        ct_screen_filepath = find_file_in_container(
            screening_session, "ct_screen_tmtv", input_dir
        )
        pt_screen_filepath = find_file_in_container(
            screening_session, "pt_screen_tmtv", input_dir
        )

        if ct_screen_filepath:
            input_files_path["ct_screening_nifti"] = ct_screen_filepath
        else:
            log.error(
                f"Not able to find corresponding ct_screening_tmtv tag in screening session container"
            )

        if pt_screen_filepath:
            input_files_path["pt_screening_nifti"] = pt_screen_filepath
        else:
            log.error(
                f"Not able to find corresponding pt_screening_tmtv tag in screening session container"
            )

    # Get followup
    ct_followup_nifti_filepath = find_file_in_container(
        followup_session_container, "ct_followup_tmtv", input_dir
    )
    if ct_followup_nifti_filepath:
        input_files_path["ct_followup_nifti"] = ct_followup_nifti_filepath
    else:
        log.error(
            f"Not able to find corresponding ct_followup_tmtv tag in screening session container"
        )

    return input_files_path


def parse_config(gear_context):
    """Parse the config.json

    Args:
        gear_context(GearToolkitContext):

    Returns:
         Tuple:
            tuple containing:
                - parent (str) Analysis container parent type
                - input_files_path (Dict[str,Any]) input file paths
                - subject_label (str) subject label for output naming
                - followup_session_label (str) Followup session label


    """
    screening_label = gear_context.config.get("screening_session_label", "")

    # Get level of run from destination's parent: subject or session
    analysis_id = gear_context.destination["id"]
    analysis = gear_context.client.get_analysis(analysis_id)
    parent = analysis.parent.type
    parents_container = analysis.parents

    subject_label = gear_context.client.get_subject(parents_container.subject).label
    followup_session_label = gear_context.client.get_session(
        parents_container.session
    ).label

    # Required input
    pet_followup_nifti_filepath = gear_context.get_input_path("pet_screening_nifti")

    if (
        gear_context.get_input_path("ct_followup_nifti")
        and gear_context.get_input_path("ct_screening_nifti")
        and gear_context.get_input_path("pet_screening_nifti")
    ):
        input_files_path = dict()
        input_files_path["ct_screening_nifti"] = gear_context.get_input_path(
            "ct_screening_nifti"
        )
        input_files_path["pet_screening_nifti"] = gear_context.get_input_path(
            "pet_screening_nifti"
        )
        input_files_path["ct_followup_nifti"] = gear_context.get_input_path(
            "ct_followup_nifti"
        )
    else:
        input_files_path = parse_input_filepath(
            parents_container, screening_label, gear_context
        )

    input_files_path["pet_followup_nifti"] = pet_followup_nifti_filepath

    return parent, input_files_path, subject_label, followup_session_label

FROM python:3.8-slim as base

# Dev install. git for pip editable install.
RUN apt-get update -qq \
    && apt-get install -y \
    git \
    curl \
    build-essential \
    cmake \
    pkg-config \
    libgdcm-tools \
    bsdtar \
    unzip \
    pigz && \
    pip install "poetry==1.0.10"

ENV PATH="/opt/niftyreg-master/bin:$PATH" \
    LD_LIBRARY_PATH="/opt/niftyreg-master/lib:$LD_LIBRARY_PATH"

RUN apt-get update -qq \
    && apt-get install -y -q --no-install-recommends \
           ca-certificates \
           cmake \
           g++ \
           gcc \
           git \
           make \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir -p /tmp/niftyreg/build \
    && git clone https://github.com/KCL-BMEIS/niftyreg /tmp/niftyreg/source \
    && cd /tmp/niftyreg/build \
    && cmake -DCMAKE_INSTALL_PREFIX=/opt/niftyreg-master -DBUILD_SHARED_LIBS=ON -DBUILD_TESTING=OFF /tmp/niftyreg/source \
    && make -j1 \
    && make install \
    && ldconfig \
    && rm -rf /tmp/niftyreg

ENV FLYWHEEL='/flywheel/v0'
RUN mkdir -p ${FLYWHEEL}
WORKDIR ${FLYWHEEL}

COPY pyproject.toml poetry.lock ./
RUN poetry install --no-dev

COPY run.py manifest.json README.md $FLYWHEEL/
COPY fw_gear_niftyreg $FLYWHEEL/fw_gear_niftyreg
RUN poetry install --no-dev


# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["poetry","run","python","/flywheel/v0/run.py"]


#!/usr/bin/env python
"""The run script"""
import logging
import sys
from pathlib import Path

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_niftyreg.main import run as niftyreg_main
from fw_gear_niftyreg.parser import parse_config

log = logging.getLogger(__name__)


def main(context: GearToolkitContext) -> None:  # pragma: no cover
    # parse the context

    parent, input_files_path, subject_label, followup_session_label = parse_config(
        context
    )

    # Validate container type
    if parent != "session":
        log.error(f"This gear must be run at the session container, not {parent} level")
        sys.exist(1)

    # Grab the work dir
    work_dir = context.work_dir
    output_folder = context.output_dir

    niftyreg_main(
        input_files_path,
        subject_label,
        followup_session_label,
        Path(output_folder).absolute(),
        Path(work_dir).absolute(),
    )


if __name__ == "__main__":  # pragma: no cover
    with GearToolkitContext() as gear_context:
        gear_context.init_logging()
        log.debug(gear_context.config_json)
        main(gear_context)

# Niftyreg
This gear is based on the NiftyReg, an open source software that is developed by members of the Centre for Medical Image Computing at University College London, UK. The gear will be used to register FDG PET/CT images. This gear will run at the session level as an analysis gear. This gear is expecting user to run in the Followup session container and provide Screening session label(default:Screening) in the Gear Configuration to be used in searching for optional inputs if they are not provided. This gear will require one input file to execute, and if other optional inputs are not provided, the gear will look for these inputs based on the following file.tag.
<br>
- __ct_screening_nifti__: ct_screen_tmtv <br>
- __pet_screening_nifti__: pt_screen_tmtv <br>
- __ct_followup_nifti__-up: ct_followup_tmtv



## Gear Inputs

### Required
* __pet_followup_nifti__ : Required. PET image from followup visit.

### Optional
* __pet_screening_nifti__ : Optional. PET image from screening visit.
* __ct_followup_nifti__ : Optional. CT image from followup visit.
* __ct_screening_nifti__ : Optional. CT image from screening visit.


### Configuration
* __screening_session_label__ (str, default "Screening"): Session label to identify the screening session container.
* __debug__(boolean, default False): Include debug statements in log.


## Gear outputs
* __res_affine_img__ : The resampled followup CT image aligned to the screening CT NIfTI image file. eg: `ct_{subject_label}_{session_label}_f2s.nii.gz`
* __res_bspline_img__ : Resampled B Spline aligned Image (image file). eg: `ct_{subject_label}_{session_label}_f2s_bspline.nii.gz`
* __bspline_pet_sp__ : Resampled B Spline aligned Image (image file). eg: `pet_{subject_label}_{session_label}_f2s_bspline.nii.gz`
* __bspline_matrix__ : NIfTI image containing computed b-spline transform. eg: `{subject_label}_{session_label}_bspline_matrix.nii.gz`
* __affine_matrix__ : Text file containing the computed affine transformation matrix, which aligns the followup image to the screening image. eg: `{subject_label}_affine_matrix.txt`
